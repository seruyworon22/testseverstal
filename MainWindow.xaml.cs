﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestSeverstal
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /*
         Постановка:
Имеется 3 поставщика, каждый из поставщиков может поставлять 2 вида груш и 2 вида яблок. Поставщики заранее сообщают свои цены на виды продукции на определенный период поставок.
Необходимо: а) создать интерфейс приемки поставок от поставщиков. В одной поставке от поставщика может быть несколько видов продукции. 
                       б) создать отчет. За выбранный период показать поступление видов продукции по поставщикам с итогами по весу и стоимости.
Требования: данные приложения должны сохранятся в БД (СУБД любая, желательно Access), клиентская часть в идеале на Delphi (VS, WEB тоже вполне подойдет).
Нам важно получить исходные коды программы + работающее мини приложение. Т.к. корпоративная политика не разрешает такое присылать на почту, то мы просим кандидатов выложить все файлы в облако и прислать ссылку.
Само приложение должно работать без ошибок, отвечать условиям задачи.
             */
        public MainWindow()
        {
            InitializeComponent();
            InitialReport();
        }
        /// <summary>
        /// Инициализация таблиц
        /// </summary>
        private  void InitialReport()
        {//это минипроект в тестовом задании, поэтому я выведу все таблицы, и не стану обрабатывать преключение вкладок или создавать для каждой вкладки отдельную страницу
            SupplierGrid.ItemsSource = Report.SuppliertList();
            ProductGrid.ItemsSource = Report.ProductList();
            PriceGrid.ItemsSource = Report.PricesList();
            SupplyGrid.ItemsSource= Report.SupplyList();
            ReportStart.SelectedDate = DateTime.Now.AddMonths(-1);
            ReportEnd.SelectedDate = DateTime.Now;
        }
        /// <summary>
        /// Добавление поставщика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupAdd_Click(object sender, RoutedEventArgs e)
        {
            //открываем окно и если установлен флаг пишем изменения и обновляем грид
                EditSup newform = new EditSup("");
                newform.ShowDialog();
            if(newform.Edit)
            {
                Report.AddSupplier(newform.Editname);
                SupplierGrid.ItemsSource = Report.SuppliertList();
            }
        }
        /// <summary>
        /// Изменение поставщика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupEdd_Click(object sender, RoutedEventArgs e)
        {
            //Находим выбранного
            Supplier sup = (Supplier)SupplierGrid.SelectedItem;
            if (sup!=null)
            {
                //открываем окно и если установлен флаг пишем изменения и обновляем грид
                EditSup newform = new EditSup(sup.Name);
                newform.ShowDialog();
                if (newform.Edit)
                {
                    Report.UPSupplier(sup.IdSupplier, newform.Editname);
                    SupplierGrid.ItemsSource = Report.SuppliertList();
                }
            }

        }
        /// <summary>
        /// Удаление поставщика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupDel_Click(object sender, RoutedEventArgs e)
        {//Находим выбранного
            Supplier sup = (Supplier)SupplierGrid.SelectedItem;
            if (sup != null)
            {//Удаляем 
                Report.DelSupplier(sup.IdSupplier);
                    SupplierGrid.ItemsSource = Report.SuppliertList();
            }
        }
        /// <summary>
        /// Добавление продукта
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProdAdd_Click(object sender, RoutedEventArgs e)
        {//открываем окно и если установлен флаг пишем изменения и обновляем грид
            EditProd newform = new EditProd("");
            newform.ShowDialog();
            if (newform.Edit)
            {
                Report.AddProduct(newform.Editname);
                ProductGrid.ItemsSource = Report.ProductList();
            }
        }
        /// <summary>
        /// Изменение продукта
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProdEdd_Click(object sender, RoutedEventArgs e)
        {//находим выбранный
            Product prod = (Product)ProductGrid.SelectedItem;
            if (prod != null)
            {//открываем окно и если установлен флаг пишем изменения и обновляем грид
                EditProd newform = new EditProd(prod.Name);
                newform.ShowDialog();
                if (newform.Edit)
                {
                    Report.UPProduct(prod.IdProduct, newform.Editname);
                    ProductGrid.ItemsSource = Report.ProductList();
                }
            }
        }
        /// <summary>
        /// Удаление продукта
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProdDel_Click(object sender, RoutedEventArgs e)
        {//находим выбранный
            Product prod = (Product)ProductGrid.SelectedItem;
            if (prod != null)
            {//удаляем
                Report.DelProduct(prod.IdProduct);
                    ProductGrid.ItemsSource = Report.ProductList();
            }
        }
        /// <summary>
        /// Удаление прайса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PriceDel_Click(object sender, RoutedEventArgs e)
        {//находим выбранный
            Prices price = (Prices)PriceGrid.SelectedItem;
            if (price != null)
            {//удаляем
                Report.DelPrice(price.IdPrice);
                PriceGrid.ItemsSource = Report.PricesList();
            }
        }
        /// <summary>
        /// Добавление прайса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PriceAdd_Click(object sender, RoutedEventArgs e)
        {//открываем окно и если установлен флаг пишем изменения и обновляем грид
            EditPrice newform = new EditPrice(null);
            newform.ShowDialog();
            if (newform.Edit)
            {
                Report.AddPrice(newform.priceEdit);
                PriceGrid.ItemsSource = Report.PricesList();
            }
        }
        /// <summary>
        /// Изменение прайса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PriceEdd_Click(object sender, RoutedEventArgs e)
        {//находим выбранный
            Prices price = (Prices)PriceGrid.SelectedItem;
            if (price != null)
            {//открываем окно и если установлен флаг пишем изменения и обновляем грид
                EditPrice newform = new EditPrice(price);
                newform.ShowDialog();
                if (newform.Edit)
                {
                    Report.UpPrice(newform.priceEdit);
                    PriceGrid.ItemsSource = Report.PricesList();
                }
            }
        }
        /// <summary>
        /// Создание поставки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddSupply_Click(object sender, RoutedEventArgs e)
        {//открываем окно и если установлен флаг пишем изменения и обновляем грид
            EditSupply newform = new EditSupply();
            newform.ShowDialog();
            if(newform.Edit)
            {
                Report.AddSupplyt(newform.editSupply, newform.ProductList);
                SupplyGrid.ItemsSource = Report.SupplyList();
            }
        }
        /// <summary>
        /// Отображение продуктов в поставке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupplyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {//находим выбранную поставку
            int i = SupplyGrid.SelectedIndex;
            if (i >= 0)
            {//выводим все продукты, которые в это поставку входят
                Supply sp = (Supply)SupplyGrid.SelectedItem;
                SupplyProductGrid.ItemsSource = Report.SupplyProdyctList(sp.IdSupply);
            }
        }
        /// <summary>
        /// Удаление поставки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DelSupply_Click(object sender, RoutedEventArgs e)
        {//находим выбранную
            Supply sp = (Supply)SupplyGrid.SelectedItem;
            if(sp!=null)
            {//Удаляем, ключ каскадно удалит все привязанные продукты
                Report.DelSupply(sp.IdSupply);
                SupplyGrid.ItemsSource = Report.SupplyList();
            }
        }
        /// <summary>
        /// Построить отчет по поставзикам
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BGetRepotr_Click(object sender, RoutedEventArgs e)
        {//Вовожу список поставщиков, сколько товара и на какую сумму они поставили товара
            ReportSupplierGrid.ItemsSource = Report.GetReportSupplier((DateTime)ReportStart.SelectedDate, (DateTime)ReportEnd.SelectedDate);
            //Встаю сразу на первую строку, чтобы заполнить и вторую таблицу
            ReportSupplierGrid.SelectedIndex = 0;
        }
        /// <summary>
        /// Отчет по продукции для поставцика
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportSupplierGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {//поличаю ид выбранного поставщика
            ReportSupplier sp = (ReportSupplier)ReportSupplierGrid.SelectedItem;
            if (sp != null)
            {//вывожу для него поставленные продукты, количество и сумарнуб цену
                ReportProduct.ItemsSource = Report.GetReportProduct((DateTime)ReportStart.SelectedDate, (DateTime)ReportEnd.SelectedDate, sp.IdSupplier);
            }
        }
    }
}
