﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Data;

namespace TestSeverstal
{
    static class Report
    {
        /// <summary>
        /// Получить лист поставщиков
        /// </summary>
        /// <returns></returns>
        static public List<Supplier> SuppliertList()
        {
            return Database.Query<Supplier>("SELECT * FROM [TestSeverstal].[dbo].[Supplier]");
        }
        /// <summary>
        /// Добавить поставщика
        /// </summary>
        /// <param name="supname"></param>наименование
        static public void AddSupplier(string supname)
        {
             Database.Execute("INSERT INTO [dbo].[Supplier] ([Name]) VALUES ('"+supname+"')");
        }
        /// <summary>
        /// Изменить поставщика
        /// </summary>
        /// <param name="id"></param>Ид изменяемого
        /// <param name="supname"></param>Новое наименование
        static public void UPSupplier(int id, string supname)
        {
            Database.Execute("UPDATE [dbo].[Supplier]   SET [Name] = '"+ supname + "' WHERE [IdSupplier]="+id.ToString()+"");
        }
        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="id"></param>//Ид удаляемого
        static public void DelSupplier(int id)
        {
            Database.Execute("delete from [dbo].[Supplier]  WHERE [IdSupplier]=" + id.ToString() + "");
        }
        /// <summary>
        /// Получить лист продукции
        /// </summary>
        /// <returns></returns>
        static public List<Product> ProductList()
        {
            return Database.Query<Product>("SELECT * FROM [TestSeverstal].[dbo].[Product]");
        }
        /// <summary>
        /// Добавить продукт
        /// </summary>
        /// <param name="supname"></param>наименование
        static public void AddProduct(string supname)
        {
            Database.Execute("INSERT INTO [dbo].[Product] ([Name]) VALUES ('" + supname + "')");
        }
        /// <summary>
        /// Изменить продукт
        /// </summary>
        /// <param name="id"></param>Ид изменяемого
        /// <param name="supname"></param>Новое наименование
        static public void UPProduct(int id, string supname)
        {
            Database.Execute("UPDATE [dbo].[Product]   SET [Name] = '" + supname + "' WHERE [IdProduct]=" + id.ToString() + "");
        }
        /// <summary>
        /// Удалить продукт
        /// </summary>
        /// <param name="id"></param>Ид удаляемого
        static public void DelProduct(int id)
        {
            Database.Execute("delete from [dbo].[Product]  WHERE [IdProduct]=" + id.ToString() + "");
        }
        /// <summary>
        /// Получать Прайслист
        /// </summary>
        /// <returns></returns>
        static public List<Prices> PricesList()
        {//мне нужны и ид для дальнейшей работы и названия для отображения. Поэтому названия я достану подзапросами
            return Database.Query<Prices>(@"SELECT p.*, (select s.Name from [Supplier] s where p.IdSupplier=s.IdSupplier) as Supplier,
                                                (select pr.Name from[Product] pr where pr.IdProduct = p.IdProduct) as Product
                                                FROM[TestSeverstal].[dbo].[Price] p");
        }
        /// <summary>
        /// Добавить цену
        /// </summary>
        /// <param name="price"></param>ээкземпляр класса прайс
        static public void AddPrice(Prices price)
        {//в классе есть вся инфа, продукт поставщик цена и период ее действия
            Database.Execute("INSERT INTO [dbo].[Price]" +
                " ([IdSupplier] " +
                ",[IdProduct] " +
                ",[StartDate] " +
                ",[EndDate] " +
                ",[Price]) " +
                " VALUES " +
                "("+price.IdSupplier+" " +
                "," + price.IdProduct + " " +
                ",'" + price.StartDate.ToShortDateString() + "' " +
                ",'" + price.EndDate.ToShortDateString() + "' " +
                "," + price.Price.ToString().Replace(',', '.') + " )");
            //В шарпе и базе разный разделитель дроби, чтобы записать дабл приходится замменять запятую на точку
        }
        /// <summary>
        /// Изменение цены
        /// </summary>
        /// <param name="price"></param>ээкземпляр класса прайс
        static public void UpPrice(Prices price)
        {// в классе есть вся инфа, продукт поставщик цена и период ее действия
            Database.Execute("UPDATE [dbo].[Price]    " +
                "SET[IdSupplier] = "+price.IdSupplier+"      " +
                ",[IdProduct] = " + price.IdProduct + "       " +
                ",[StartDate] = '" + price.StartDate.ToShortDateString() + "'      " +
                ",[EndDate] = '" + price.EndDate.ToShortDateString() + "'      " +
                ",[Price] = " + price.Price.ToString().Replace(',', '.') + " " +
                "WHERE IdPrice= "+ price.IdPrice + " ");
        }
        /// <summary>
        /// Удалить цену
        /// </summary>
        /// <param name="id"></param>Ид удаляемой строки
        static public void DelPrice(int id)
        {
            Database.Execute("delete from [dbo].[Price]  WHERE [IdPrice]=" + id.ToString() + "");
        }
        /// <summary>
        /// Получить лист поставок
        /// </summary>
        /// <returns></returns>
        static public List<Supply> SupplyList()
        {
            return Database.Query<Supply>("SELECT [Supply].*, (select s.Name from [Supplier] s where [Supply].IdSupplier=s.IdSupplier) as Supplier FROM [TestSeverstal].[dbo].[Supply]");
        }
        /// <summary>
        /// Добавить  поставку
        /// </summary>
        /// <param name="sup"></param>ээкземпляр класса Supply( Дата и поставщик)
        /// <param name="ProductList"></param> лист класса SupplyProducts (списооок продуктов)
        static public void AddSupplyt(Supply sup, ObservableCollection<SupplyProducts> ProductList)
        {//создаем в базе поставку
            Database.Execute("INSERT INTO [dbo].[Supply] ([IdSupplier] ,[DateSupply]) VALUES ("+ sup.IdSupplier + " ,'"+ sup.DateSupply.ToShortDateString() + "')");
            //получаем последнюю ид
            List<int> i= Database.Query<int>(@"SELECT IDENT_CURRENT('Supply')");
            for(int q=0; q< ProductList.Count; q++)
            {// и для каждого продукта в листе пишем его с привязкой к поставке
                Database.Execute("INSERT INTO [dbo].[SupplyProduct]  ([IdSupply]  ,[IdProduct]  ,[CountProduct])   " +
                    " VALUES("+i[0]+"           ,"+ ProductList[q].IdProduct + "          ,"+ ProductList[q].CountProduct + ")");
            }
        }
        /// <summary>
        /// Получить лист проодукции в поставке
        /// </summary>
        /// <param name="id"></param>ид поставки
        /// <returns></returns>
        static public List<SupplyProducts> SupplyProdyctList(int id)
        {
            return Database.Query<SupplyProducts>("SELECT [SupplyProduct].*, (select s.Name from [Product] s where [SupplyProduct].IdProduct=s.IdProduct) as Product FROM [TestSeverstal].[dbo].[SupplyProduct] where idSupply= "+id+"");
        }
        //Удалить поставку
        static public void DelSupply(int id)
        {//Ключ каскадно удалит продукты
            Database.Execute("delete from [dbo].[Supply]  WHERE [IdSupply]=" + id.ToString() + "");
        }
        /// <summary>
        /// Получить отчет по поставщикам
        /// </summary>
        /// <param name="start"></param>Дата начала
        /// <param name="end"></param>Дата конца
        /// <returns></returns>
        static public List<ReportSupplier> GetReportSupplier(DateTime start, DateTime end)
        {
            //я беру все поставки которые подходят под условие дат. Соединяю с продукцией и для каждого продукта подзапросом нахожу цену.
            //если вдруг в прайсе нет значения цены для этой даты, то я беру период который бил раньше
            //группирую по поставщику и нахожу количество поставленного товара и суммарную цену
            //подзапросом достаю наименование поставщиков
            List<ReportSupplier> res= Database.Query<ReportSupplier>("Select t.IdSupplier, (select s.Name from Supplier s where s.IdSupplier=t.IdSupplier) as Supplier, sum(t.CountProduct) as AllProduct, sum(t.CountProduct*Price) as AllPrice from ( " +
                "SELECT su.*, sp.CountProduct, (select Top(1) p.Price from price p where p.IdProduct = sp.IdProduct and p.IdSupplier = su.IdSupplier and p.StartDate < su.DateSupply order by p.StartDate desc) as Price " +
                "  FROM[TestSeverstal].[dbo].[SupplyProduct] sp, [TestSeverstal].[dbo].[Supply] su " +
                " where sp.IdSupply=su.IdSupply  and su.DateSupply>='"+ start.ToShortDateString() + "' and su.DateSupply<'"+end.ToShortDateString() + "') t " +
                " group by t.IdSupplier");
            //Собираю строку итого
            ReportSupplier total = new ReportSupplier();
            total.IdSupplier = -1;
            total.Supplier = "Всего:";
            for(int i=0; i< res.Count(); i++)
            {
                total.AllPrice += res[i].AllPrice;
                total.AllProduct += res[i].AllProduct;
            }
            res.Add(total);
            return res;
        }
        /// <summary>
        /// Получить отчет по продукции для поставщика
        /// </summary>
        /// <param name="start"></param>Дата начала
        /// <param name="end"></param>Дата конца
        /// <param name="supplier"></param>Поставщик
        /// <returns></returns>
        static public List<ReportProduct> GetReportProduct(DateTime start, DateTime end, int supplier)
        {
            //я беру все поставки которые подходят под условие дат и поставлены выбранным поставщиком. Соединяю с продукцией и для каждого продукта подзапросом нахожу цену.
            //если вдруг в прайсе нет значения цены для этой даты, то я беру период который бил раньше
            //группирую по продукту и нахожу количество товара и суммарную цену
            //подзапросом достаю наименование продукта
            string sql = "select t.IdProduct, (select s.Name from Product s where s.IdProduct=t.IdProduct) as Product, sum(t.CountProduct) as AllProduct, sum(t.CountProduct*Price) as AllPrice from ( " +
                " SELECT su.DateSupply, sp.*, (select Top(1) p.Price from price p where p.IdProduct = sp.IdProduct and p.IdSupplier = su.IdSupplier and p.StartDate < su.DateSupply order by p.StartDate desc) as Price  " +
                " FROM[TestSeverstal].[dbo].[SupplyProduct] sp, [TestSeverstal].[dbo].[Supply] su " +
                " where sp.IdSupply=su.IdSupply ";
            if (supplier > 0)
            {//Указан поставщик, возьму все его товары
                sql += " and su.IdSupplier=" + supplier + "";
            }
            else 
            {//выбрана строка "Всего", возьму товары для всех поставщиков
                sql += " and su.IdSupplier is not null";
            }
                sql+=" and su.DateSupply>='" + start.ToShortDateString() + "' and su.DateSupply<'" + end.ToShortDateString() + "') t " +
                " group by t.IdProduct";

            
            List<ReportProduct> res = Database.Query<ReportProduct>(sql);
            // Собираю строку итого
            ReportProduct total = new ReportProduct();
            total.IdProduct = -1;
            total.Product = "Всего:";
            for (int i = 0; i < res.Count(); i++)
            {
                total.AllPrice += res[i].AllPrice;
                total.AllProduct += res[i].AllProduct;
            }
            res.Add(total);
            return res;
        }

    }
    /// <summary>
    /// Поставщик
    /// </summary>
    public class Supplier
    {
        /// <summary>
        /// Первичный ключ 
        /// </summary>
        public int IdSupplier { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
    }
    /// <summary>
    /// Отчет для поставщика
    /// </summary>
    public class ReportSupplier
    {//Это класс для отчета
        /// <summary>
        /// Тд поставзика
        /// </summary>
        public int IdSupplier { get; set; }
        /// <summary>
        /// Наименование поставщика
        /// </summary>
        public string Supplier { get; set; }
        /// <summary>
        /// количество поставленного продукта
        /// </summary>
        public int AllProduct { get; set; }
        /// <summary>
        /// Суммарная цена продуктов
        /// </summary>
        public double AllPrice { get; set; }
    }
    /// <summary>
    /// Отчет по продукции
    /// </summary>
    public class ReportProduct
    {//это класс для отчета
        /// <summary>
        /// Ид продукции
        /// </summary>
        public int IdProduct { get; set; }
        /// <summary>
        /// Наименование продукции
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// Всего поставлено продуктов
        /// </summary>
        public int AllProduct { get; set; }
        /// <summary>
        /// Суммарная цена продуктов
        /// </summary>
        public double AllPrice { get; set; }
    }
    /// <summary>
    /// Поставка
    /// </summary>
    public class Supply
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public int IdSupply { get; set; }
        /// <summary>
        /// Внешний ключ на поставщика, при удалении присвоит NULL
        /// </summary>
        public int IdSupplier { get; set; }
        /// <summary>
        /// Наименование из подзапроса
        /// </summary>
        public string Supplier { get; set; }
        /// <summary>
        /// Дата поставки
        /// </summary>
        public DateTime DateSupply { get; set; }
    }
    /// <summary>
    /// Продукция
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public int IdProduct { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
    }
    /// <summary>
    /// Цена
    /// </summary>
    public class Prices
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public int IdPrice { get; set; }
        /// <summary>
        /// Внешний ключ на поставшика, каскадное удаление
        /// </summary>
        public int IdSupplier { get; set; }
        /// <summary>
        /// Наименование поставщика из подзапроса
        /// </summary>
        public string Supplier { get; set; }
        /// <summary>
        /// Внешний ключ на продукцию, каскадное удаление
        /// </summary>
        public int IdProduct { get; set; }
        /// <summary>
        /// Наименование продукции из подзапроса
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// Начало действия цены
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Конец действия цены
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Цена
        /// </summary>
        public double Price { get; set; }
    }
    /// <summary>
    /// Продукт в поставке
    /// </summary>
    public class SupplyProducts
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        public int IdSupplyProduct { get; set; }
        /// <summary>
        /// Внешний ключ на поставку, каскадное удаление
        /// </summary>
        public int IdSupply { get; set; }
        /// <summary>
        /// Внешний ключ на проодукт, при удалении присвоит NULL
        /// </summary>
        public int IdProduct { get; set; }
        /// <summary>
        /// Наименование из подзапроса
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// Количество/Вес продукции в поставке
        /// </summary>
        public int CountProduct { get; set; }

    }

}
