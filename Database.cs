﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TestSeverstal
{

    static class Database
    {
        /// <summary>
        /// Генерирует строку подключения
        /// </summary>
        public static string ConnectionString
        {
            get
            {//у меня несколько серверов, проверил на 2
                //return @" pooling = false; Data Source = DESKTOP-5190KJP\SQLEXPRESS; Initial Catalog = TestSeverstal; Integrated Security = True";
                return @" pooling = false; Data Source = localhost; Initial Catalog = TestSeverstal; Integrated Security = True";
            }

        }
        /// <summary>
        /// Выполняет селект запрос базе
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param> текст селект запроса
        /// <param name="param"></param> параметры
        /// <returns></returns>Возвращает лист указанного класса
        public static List<T> Query<T>(string sql, object param = null)
        {
            var res = new List<T>();
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                    res.AddRange(db.Query<T>(sql, param));
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
            return res;
        }
        /// <summary>
        /// Выполняет запрос модификации данных
        /// </summary>
        /// <param name="sql"></param>текст запроса
        /// <param name="param"></param>параметры
        /// <param name="command"></param>
        public static void Execute(string sql, object param = null, CommandType? command = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnectionString))
                    db.Execute(sql, param, commandType: command);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }
    }
}
