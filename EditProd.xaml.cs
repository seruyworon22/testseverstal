﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestSeverstal
{
    /// <summary>
    /// Логика взаимодействия для EditProd.xaml
    /// </summary>
    public partial class EditProd : Window
    {
        //флаг
        public bool Edit = false;
        //название продукта
        public string Editname;
        public EditProd(string supname)
        {
            InitializeComponent();
            //если что-то передали то вывести для редакции
            TBname.Text = supname;
        }
        /// <summary>
        /// Принять изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BAccept_Click(object sender, RoutedEventArgs e)
        {
            ///ставим флаг
            Edit = true;
            //читаем имя с интерфейса
            Editname = TBname.Text;
            this.Close();
        }
        /// <summary>
        /// Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BCancel_Click(object sender, RoutedEventArgs e)
        {
            Edit = false;
            this.Close();
        }
    }
}
