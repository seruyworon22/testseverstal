﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace TestSeverstal
{
    /// <summary>
    /// Логика взаимодействия для EditSupply.xaml
    /// </summary>
    public partial class EditSupply : Window
    {
        /// <summary>
        /// флаг изменений
        /// </summary>
        public bool Edit = false;
        /// <summary>
        /// поставка
        /// </summary>
        public Supply editSupply = new Supply();
        /// <summary>
        /// продукты в поставке
        /// </summary>
        public ObservableCollection<SupplyProducts> ProductList = new ObservableCollection<SupplyProducts>();
        //продукты должны быть привязаны к поставке, поэтому сначала я собираю их в лист, а запищу в базу после создания поставки 
        public EditSupply()
        {
            InitializeComponent();
            //справочник поставщиков
            Sup.ItemsSource = Report.SuppliertList();
            DataSup.SelectedDate = DateTime.Now;
            SupplyGrid.ItemsSource = ProductList;
        }
        /// <summary>
        /// Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Принять
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Supplier selectedSup = (Supplier)Sup.SelectedItem;
                if ( selectedSup != null)
                {//Проверяем что поставщик выбран и собираем в класс
                    editSupply.IdSupplier = selectedSup.IdSupplier;
                    editSupply.DateSupply = DataSup.SelectedDate.Value;
                    Edit = true;
                    this.Close();
                }
                else { System.Windows.MessageBox.Show("Не выбран поставщик"); }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }

        }
        /// <summary>
        /// Добавить продукт в поставку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {//открываем окно для внесения продукта
            SupplyProduct newform = new SupplyProduct();
            newform.ShowDialog();
            if (newform.Edit)
            {//если флаг установлен, то добавляем продукт в лист
                ProductList.Add(newform.sp);
            }
        }
        /// <summary>
        /// Удалить продукт из поставки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {//берем ид выбранной строки
            int i = SupplyGrid.SelectedIndex;
            if(i>=0)
            {//если чтото выбрано удаляем из листа
                ProductList.RemoveAt(i);
            }
        }
    }
}
