﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestSeverstal
{
    /// <summary>
    /// Логика взаимодействия для SupplyProduct.xaml
    /// </summary>
    public partial class SupplyProduct : Window
    {
        /// <summary>
        /// Продукт в поставке
        /// </summary>
        public SupplyProducts sp = new SupplyProducts();
        /// <summary>
        /// флаг изменения
        /// </summary>
        public bool Edit = false;
        public SupplyProduct()
        {
            InitializeComponent();
            //справочник продуктов
            Prod.ItemsSource = Report.ProductList();
        }
        /// <summary>
        /// Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Принять изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Product selectedProd = (Product)Prod.SelectedItem;
                //проверяем выбран ли продукт
                if (selectedProd != null)
                {//собираем экземпляр класса
                    sp.IdProduct = selectedProd.IdProduct;
                    sp.Product = selectedProd.Name;
                    sp.CountProduct = Convert.ToInt32(TBCost.Text);
                    //ставим флаг
                    Edit = true;
                    this.Close();
                }
                else { System.Windows.MessageBox.Show("Не выбран продукт"); }
            }
            catch (Exception ex)
            {//текст в поле количества
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }
    }
}
