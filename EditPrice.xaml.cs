﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace TestSeverstal
{
    /// <summary>
    /// Логика взаимодействия для EditPrice.xaml
    /// </summary>
    public partial class EditPrice : Window
    {
        //Изменяемый прайс
        public Prices priceEdit;
        //флаг изменений
        public bool Edit = false;
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="price"></param>Прайс для изменения
        public EditPrice(Prices price)
        {
            InitializeComponent();
            //принимаем в обработку переданный прайс
            priceEdit = price;
            //подтягиваем в комбобоксы справочники
            Sup.ItemsSource= Report.SuppliertList();
            Prod.ItemsSource= Report.ProductList();
            //переданный прайс не пустой, мы меняем а не создаем новый
            if (price != null)
            {
                ////Я чегото видимо не допонимаю и SelectedItem отказывается принимать экземпляр класса
                ///Разбираться с этим сейчас нет времени, поэтому будут костыли
                ///Дешево сердито, но зато работает
                Sup.SelectedIndex = crutch1(Report.SuppliertList(), price.IdSupplier); 
                Prod.SelectedIndex = crutch2(Report.ProductList(), price.IdProduct);
                //подтягиваем из переданного прайса инфу
                StartDP.SelectedDate = price.StartDate;
                EndDP.SelectedDate = price.EndDate;
                TBCost.Text = price.Price.ToString();
            }
            else
            {//переданный прайс пуст, мы создаем новый
                priceEdit = new Prices();//пустой экземпляр класса
                // в датапикеры сегодняшнее число 
                StartDP.SelectedDate = DateTime.Now;
                EndDP.SelectedDate = DateTime.Now;
            }
        }
        /// <summary>
        /// Отмена редактирования
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// определяет выбранного поставщика 
        /// </summary>
        /// <param name="List"></param> Лист поставщиков
        /// <param name="selectedId"></param> выбранный ид
        /// <returns></returns>
        private int crutch1(List<Supplier> List, int selectedId)
        {///если я не могу подсунуть выбранный класс, я могу определить по какому индексу этот класс пришел в комбобокс
            ///Для этого я пройду по листу и найду выбранную ид
            int res = 0;
            for (int i = 0; i < List.Count; i++)
            {
                if(List[i].IdSupplier== selectedId)
                { res = i; break; }
            }
            return res;
        }
        /// <summary>
        /// определяет выбранный товар
        /// </summary>
        /// <param name="List"></param> Лист продукции
        /// <param name="selectedId"></param> выбранный ид
        /// <returns></returns>
        private int crutch2(List<Product> List, int selectedId)
        {///логика таже, что с поставщиком, найти индекс с которым класс пришел в комбобокс
            int res = 0;
            for (int i = 0; i < List.Count; i++)
            {
                if (List[i].IdProduct == selectedId)
                { res = i; break; }
            }
            return res;
        }
        /// <summary>
        /// Принять изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try 
            {//разбираем комбобоксы на итемы
                Supplier selectedSup = (Supplier)Sup.SelectedItem;
                Product selectedProd = (Product)Prod.SelectedItem;
                //проверяем что в них есть значения
                if (selectedProd!=null && selectedSup!=null) 
                {
                    if (StartDP.SelectedDate.Value<= EndDP.SelectedDate.Value)
                    {//проверяем что даты в правильном порядке
                        //собираем новый прайс с интерфейса
                        priceEdit.IdSupplier = selectedSup.IdSupplier;
                        priceEdit.IdProduct = selectedProd.IdProduct;
                        priceEdit.StartDate = StartDP.SelectedDate.Value;
                        priceEdit.EndDate = EndDP.SelectedDate.Value;
                        priceEdit.Price = Convert.ToDouble(TBCost.Text);
                        //ставим флаг редактирования 
                        Edit = true;
                        this.Close();
                    }
                    else { System.Windows.MessageBox.Show("Конец периода раньше начала"); }
                }
                else { System.Windows.MessageBox.Show("Не выбран поставщик или продукт"); }
            }
            catch (Exception ex)
            {//кетч на случай некоректных данных в цене
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }
    }


}
